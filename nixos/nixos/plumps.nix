{config, lib, pkgs, ... }:

let _config = import ../config { inherit pkgs; };

in
{   
    boot.kernelPackages = pkgs.linuxPackages_latest;
    
    environment.etc = _config.environment_etc;
    environment.systemPackages = with pkgs;
        _config.system_packages ++
        [   _config.theme_switch
            termite.terminfo
            xsel # needed for neovim to support copy/paste
            gnumake
            ngrok
            unrar
            unzip
            wget
            which

            # nix tools
            nixops
            nodePackages.node2nix
        ];
    
    networking.extraHosts = ''
        127.0.0.1 ${config.networking.hostName}
        ::1 ${config.networking.hostName}
    '';
    
    nixpkgs.config.allowBroken = false;
    nixpkgs.config.allowUnfree = true;
    nixpkgs.config.allowUnfreeRedistributable = true;
    nixpkgs.config.packageOverrides = pkgs: (import ./../pkgs { inherit pkgs _config; });
  
    programs.bash.enableCompletion = true;
    programs.ssh.forwardX11 = false;
    programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
    programs.zsh.enable = true;
    
    services.locate.enable = true;
    services.transmission.enable = true;
  
    systemd.extraConfig = ''
        DefaultCPUAccounting=true
        DefaultBlockIOAccounting=true
        DefaultMemoryAccounting=true
        DefaultTasksAccounting=true
    '';
    
    users.users."plumps" = {
        hashedPassword = "$6$tts2fxQSr9H$gFfJnBPghh7S3KySWx6l3CeqqtjN8atZ4/lTSJNZoOEDMcKv/m4pO5Hwdi.moE1yajprasQx60VZeEYxTQSpH.";
        isNormalUser = true;
        uid = 1000;
        description = "Maksim Bronsky";
        extraGroups = [ "wheel" "vboxusers" "networkmanager" "plugdev" "audio" "video" "netdev" "lp" ] ;
        group = "users";
        home = "/home/plumps";
    };

    time.timeZone = "Europe/Berlin";
    
    virtualisation.virtualbox.host.enable = true;
    virtualisation.docker.enable = true;
}
