{config, lib, pkgs, ... }:

let _config = import ../config { inherit pkgs; };

in
{   

    environment.systemPackages = with pkgs;
        _config.system_packages ++
        [   firefox
            chromium
            vlc
            tdesktop
            kdeApplications.kate
            kdeApplications.akonadiconsole
	    kdeApplications.okular
	    kdeApplications.ark
            kdeApplications.kdepim-runtime
            kdeApplications.kdepim-apps-libs
            kdeApplications.libkdepim
            kdeApplications.kmail
            kdeApplications.kontact
            kdeApplications.kontactinterface
            kdeApplications.korganizer
            kdeApplications.akregator
            yakuake
	    calibre
            transgui
	    thunderbird
        ];
        
    fonts.enableFontDir = true;
    fonts.enableGhostscriptFonts = true;
    fonts.fonts = with pkgs; [
        anonymousPro
        #corefonts
        dejavu_fonts
        freefont_ttf
        liberation_ttf
        source-code-pro
        terminus_font
        #ttf_bitstream_vera
        nerdfonts
    ];

    nixpkgs.config.packageOverrides = pkgs: (import ./../pkgs { inherit pkgs _config; });
    nixpkgs.config.firefox.enableAdobeFlash = true;
    nixpkgs.config.firefox.enableGoogleTalkPlugin = true;
    nixpkgs.config.firefox.jre = false;
    nixpkgs.config.zathura.useMupdf = true;

    services.dbus.enable = true;
    services.redshift.enable = true;
    services.redshift.latitude = "51.5";
    services.redshift.longitude = "12";
    services.xserver.enable = true;
    services.xserver.layout = "de";
    services.xserver.xkbVariant = "neo";
    services.xserver.libinput.enable = true;
    services.xserver.displayManager.sddm.enable = true;
    services.xserver.desktopManager.plasma5.enable = true;
}
