{config, ...}:
{
    imports = [ ./lenovo.nix ];
    
    # enable optirun
    hardware.bumblebee.enable = true;
    
    services.thinkfan.sensor = "/sys/class/hwmon/hwmon0/temp1_input";
}
