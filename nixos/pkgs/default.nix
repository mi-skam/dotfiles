{ pkgs
, _config ? null
}:

let

  self = { 

    inherit _config;

    weechat = pkgs.weechat.override {
      extraBuildInputs = [ pkgs.pythonPackages.websocket_client ];
    };

    termite = pkgs.termite.override { configFile = "/tmp/config/termite"; };

#     neovim = pkgs.neovim.override {
#       vimAlias = true;
#       configure = import ./nvim_config.nix {
#         inherit pkgs;
#         theme = if _config == null then null else _config.theme;
#       };

  };

in self
